<?php 
    Global $URLargs;
?>
<?php if( in_array($URLargs[1], array('it', 'demo'))): ?>
    <!-- Redirect to login.php if not logged in (or demo) -->
    <?php if(($auth ? !$auth->isValid() : true) && $URLargs[1]!='demo'): ?>
        <script>
        //Create a dialog window
        function MyDialog( config ) {
            MyDialog.super.call( this, config );
        }
        OO.inheritClass( MyDialog, OO.ui.Dialog );
        MyDialog.static.name = 'myDialog';
        MyDialog.static.title = 'Log in';
        MyDialog.static.escapable = false;

        // Customize the initialize() method to add content and actions.
        MyDialog.prototype.initialize = function () {
            MyDialog.super.prototype.initialize.call( this );
            this.content = new OO.ui.PanelLayout( { padded: true, expanded: false, scrollable: false } );
            this.content.$element.append( '<p><?php echo new Tutor\Message('login-required')?><br/><br/>' ); //Text
            var button= new OO.ui.ButtonGroupWidget( { //Buttons
                items: [
                new OO.ui.ButtonWidget( {
                    label: '<?php echo str_replace("$1", "Wikimedia", strval(new Tutor\Message('login-with')))?>',
                    icon: 'userAvatarOutline',
                    flags: [
                    'progressive'
                    ],
                    href: 'login.php?returnto=<?php echo basename($_SERVER['REQUEST_URI'])?>'
                } ),
                new OO.ui.ButtonWidget( {
                    label: '<?php echo new Tutor\Message('demowiki')?>',
                    href: 'demo'
                } )
                ]
            } );  

            this.content.$element.append( button.$element);
            this.$body.append( this.content.$element );
        };
        MyDialog.prototype.getBodyHeight = function () {
            return this.content.$element.outerHeight( true )+1;
        };
        var myDialog = new MyDialog( {
            size: 'medium',
        } );
        // Create and append a window manager, which opens and closes the window.
        var windowManager = new OO.ui.WindowManager();
        $( document.body ).append( windowManager.$element );
        windowManager.addWindows( [ myDialog ] );
        // Open the window
        windowManager.openWindow( myDialog );
        </script>
    <?php endif ?>
    <!-- Display tools -->
    <?php
        $indexLayout = new OOUI\IndexLayout( [
        'infusable' => true,
        'expanded' => false,
        'framed' => false,
        'id' => 'indexLayout'
        ] );
        $indexLayout->addTabPanels( [
        new OOUI\TabPanelLayout( [
            'framed' => false,
            'name' => 'panel1',
            'label' => strval(new Tutor\Message('reassign-mentees')),
            'content' => array(
            new OOUI\HtmlSnippet( strval(new Tutor\Message('reassign-mentees-mentor-name'))),
            new OOUI\TextInputWidget( [
                'placeholder' => strval(new Tutor\Message('username')),
                'infusable' => true,
                'id' => 'reassign-mentees-mentor',
                'autofocus' => true,
                'autocomplete' => false,
                'type' => 'text',
                'spellcheck' => false
            ] ),
            new OOUI\HtmlSnippet('<br/><br/>'),
            new OOUI\ButtonWidget( [
                'label' => strval(new Tutor\Message('reassign-mentees-button')),
                'flags' => [ 'primary', 'progressive' ],
                'infusable' => true,
                'id' => 'reassign-mentees-button'
            ] ),
            ),
            'expanded' => false,
            'tabItemConfig' => [
            'href' => '#reassign-mentees'
            ],
            'classes' => ['overflow']
        ] ),
        new OOUI\TabPanelLayout( [
            'framed' => false,
            'name' => 'panel2',
            'label' => strval(new Tutor\Message('reassign-mentor')),
            'content' => array(
                new OOUI\HtmlSnippet( strval(new Tutor\Message('reassign-mentor-mentee-name'))),
                new OOUI\TextInputWidget( [
                    'placeholder' => strval(new Tutor\Message('username')),
                    'infusable' => true,
                    'id' => 'reassign-mentor-mentee',
                    'autofocus' => true,
                    'autocomplete' => false,
                    'type' => 'text',
                    'spellcheck' => false
                ] ),
                new OOUI\HtmlSnippet('<br/><br/>'),
                new OOUI\ButtonWidget( [
                    'label' => strval(new Tutor\Message('reassign-mentees-button')),
                    'flags' => [ 'primary', 'progressive' ],
                    'infusable' => true,
                    'id' => 'reassign-mentor-button'
                ] ),
                ),
            'expanded' => false,
            'tabItemConfig' => [
                'href' => '#reassign-mentor'
            ],
            'classes' => ['overflow']    
        ] )
        ] );
        echo $indexLayout;
    
    ?>
    <script>
        var indexLayout = OO.ui.infuse($('#indexLayout'));
        var mentorInput = OO.ui.infuse($('#reassign-mentees-mentor'));
        var mentorReassignButton = OO.ui.infuse($('#reassign-mentees-button'));
        mentorReassignButton.on('click', function(){
            mentorReassignButton.setDisabled(true);

            let data = {'action': "reassign-mentee", 'mentor': mentorInput.getValue(), 'wiki': '<?php echo $URLargs[1]?>'};
            $.post('api.php', data, function(data,status){
                mentorReassignButton.setDisabled(false);
                console.log(data);
                var result = new OO.ui.MessageWidget( {
                    type: JSON.parse(data).status,
                    label: JSON.parse(data).message
                } );
                // Append the button to the DOM.
                $( document.getElementById("reassign-mentees-mentor").parentElement ).prepend( result.$element );
            });
        });
        var menteeInput = OO.ui.infuse($('#reassign-mentor-mentee'));
        var menteeReassignButton = OO.ui.infuse($('#reassign-mentor-button'));
        menteeReassignButton.on('click', function(){
            menteeReassignButton.setDisabled(true);

            let data = {'action': "reassign-mentor", 'mentee': menteeInput.getValue(), 'wiki': '<?php echo $URLargs[1]?>'};
            $.post('api.php', data, function(data,status){
                menteeReassignButton.setDisabled(false);
                console.log(data);
                var result = new OO.ui.MessageWidget( {
                    type: JSON.parse(data).status,
                    label: JSON.parse(data).message
                } );
                // Append the button to the DOM.
                $( document.getElementById("reassign-mentor-mentee").parentElement ).prepend( result.$element );
            });
        });

    </script>
<?php else: ?>      
    <!-- Invalid wiki -->
    <strong>Forbidden.</strong>

<?php endif ?>