<?php

namespace Tutor;

require_once $_SERVER['DOCUMENT_ROOT'].'/../vendor/autoload.php';


\OOUI\Theme::setSingleton( new \OOUI\WikimediaUITheme() );
\OOUI\Element::setDefaultDir( 'ltr' );

session_start();
$auth=$_SESSION['auth'] ? unserialize($_SESSION['auth']) : null;

// Retrieve enviriornmental variables
$dotenv = \Dotenv\Dotenv::createImmutable($_SERVER['DOCUMENT_ROOT']."/..");
$dotenv->safeLoad();

$configFiles = [
    'ui' => $_SERVER['DOCUMENT_ROOT'].'/ui.json',
    'warning' => $_SERVER['DOCUMENT_ROOT'].'/../warning.txt'
];

class Message{
    private $id;
    public function __construct($id){
        $this->id=$id;
    }
    public function __toString(){
        global $configFiles;
        $messages = json_decode(file_get_contents($configFiles['ui']), true);
        return $messages[$this->id][substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2)] ?? $messages[$this->id]['en'];
    }

}

class Auth{
    protected $username;
    protected $ip;

    public function __construct($username){
        $this->username=$username;
        $this->ip=$_SERVER['REMOTE_ADDR'];
    }

    public function isValid(){
        return $this->username != null;
    }

    public function getUsername(){
        if($this->isValid()){
            return $this->username;
        } else return null;
    }

}

class OAuthClient{
    protected \MediaWiki\OAuthClient\Client $client;
    public function __construct(){
        global $configFiles;
        if ( ($_ENV["OAUTH_URL"] ? false : true) && ($_ENV["OAUTH_CONSUMER_KEY"] ? false : true) && ($_ENV["OAUTH_CONSUMER_SECRET"] ? false : true)) {
            echo "Configuration could not be read. Please create .env by copying .env.example.";
            exit( 1 );
        }

        // Configure the OAuth client with the URL and consumer details.
        $conf = new \MediaWiki\OAuthClient\ClientConfig( $_ENV["OAUTH_URL"] );
        $conf->setConsumer( new \MediaWiki\OAuthClient\Consumer( $_ENV["OAUTH_CONSUMER_KEY"], $_ENV["OAUTH_CONSUMER_SECRET"] ) );
        $conf->setUserAgent( 'BOTutor/1.0' );
        
        $client = new \MediaWiki\OAuthClient\Client( $conf );

        $client->setCallback($_ENV["HOSTNAME"].'/login.php?returnto='.( $_GET['returnto'] ?? '/'));

        $this->client=$client;
    }

    public function getLoginURL(){
        $login = $this->client->initiate();

        // Store the Request Token in the session. We will retrieve it from there when the user is sent back
        // from the wiki (see demo/callback.php).
        $_SESSION['request_key'] = $login[1]->key;
        $_SESSION['request_secret'] = $login[1]->secret;

        return $login[0];
    }

    public function getMWClient(){
        return $this->client;
    }
}

class OAuth extends Auth{
    protected $token;
    protected $client;
    public function __construct($requestToken, $oauthVerifier){
        $client=new OAuthClient();

        // Send an HTTP request to the wiki to retrieve an Access Token.
        $accessToken = $client->getMWClient()->complete( $requestToken,  $oauthVerifier );

        // You also no longer need the Request Token.

        // Make the api.php URL from the OAuth URL.
        $apiUrl = preg_replace( '/index\.php.*/', 'api.php', $oauthUrl );

        $this->token = new \MediaWiki\OAuthClient\Token( $accessToken->key, $accessToken->secret);
        $this->client=$client;
        parent::__construct($client->getMWClient()->identify($this->token)->username);
    }

    private function getClient(){
        return $this->client;
    }

    public function isValid(){
        try{
            $client = new OAuthClient();
            $ident = $client->getMWClient()->identify( $this->token ); //TODO: CHECK ERR IF EXPIRED
            return true && $this->username == $ident->username;
        } catch (MWException $e) {
            return false;
        }
    }

    public function APICall($url, $isPost=false, $data=[]){
        return $this->getClient()->getMWClient()->makeOAuthCall($this->token, $url, $isPost, $data);
    }
}

class APIAuth extends Auth{

}