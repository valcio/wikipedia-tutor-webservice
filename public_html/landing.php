<?php
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions.php';
?><h2>
    <?php echo new Tutor\Message('choose-wiki') ?>
</h2>
<?php
$dropdown = new OOUI\DropdownInputWidget( [
    'options' => [
    [ 'data' => 'it', 'label' => strval(new Tutor\Message('itwiki')) ],
    [ 'data' => 'demo', 'label' => strval(new Tutor\Message('demowiki')) ]
    ],
    'infusable' => true,
    'id' => 'wiki'
] );

$button = new OOUI\ButtonWidget( [
    'label' => strval(new Tutor\Message('go')),
    'flags' => [ 'primary', 'progressive' ],
    'infusable' => true,
    'id' => 'go',
    'href' => 'l='
] );

echo new OOUI\ActionFieldLayout($dropdown, $button)
?>
<script>
    var myButton = OO.ui.infuse( $('#go') );
    var dropdown=OO.ui.infuse($('#wiki'));
    myButton.on( 'click', function () {
        window.location.href="/"+dropdown.getValue();
    } );
</script>